sed -i 's/\r$//' mount.sh

TEMPFILE=filelocation

function folderCreate {
	local channelId=${1}
	local sourcePath=${2}
	
	if [ -L "$TEMPFILE$channelId" ] ; then
		lnControl="$(ls -l $TEMPFILE$channelId | awk '{print $11}')"
		if [ ! "$lnControl" == "$sourcePath" ] ; then
			rm -rf "$TEMPFILE$channelId"
			echo "Wrong Link: $TEMPFILE$channelId##################"
			ln -s "$sourcePath" "$TEMPFILE$channelId"
			echo "Link Fixed $sourcePath=>$TEMPFILE$channelId"
		#else 
			
			#echo "Mount OK : $lnControl=$sourcePath"
		fi	
	else
		ln -s "$sourcePath" "$TEMPFILE$channelId"
		echo "Linked : $sourcePath=>$TEMPFILE$channelId"	
			
	fi
}

function mountCheck {
	local sourceFolder=${1}
	local targetFolder=${2}
	mountControl="$(sudo df -h $targetFolder | awk 'NR>1{print $1}')"
	# Mount Point exists
	if mountpoint -q $targetFolder
	then
		if [ ! "$mountControl" == "$sourceFolder" ] ; then
			umount -f $targetFolder 
			echo "Wrong Mount: $mountControl=>$sourceFolder"
			mount -t nfs $sourceFolder $targetFolder
			echo "Mount Fixed $sourceFolder=>$targetFolder"
		#else 
			#echo "Mount OK : $mountControl=$sourceFolder"
		fi
	else
		mount -t nfs $sourceFolder $targetFolder
		echo "Mounted : $sourceFolder=>$targetFolder"
	fi
	}
 
 
mountCheck "sambalocation" "mountlocation"


folderCreate filename "mountlocation"  
