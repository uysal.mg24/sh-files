#!/bin/sh

function Check {
        local title=${1}
        local info=${2}
        echo -e "" $title  $info
}

Check   "OS             :" "$(lsb_release -d | awk '{print $2 " " $3}')"
Check   "Uptime         :" "$(uptime -p | awk '{print $2 " " $3}')"
Check   "Disk           :" "$(df -h / | awk '{print $3}' | tail -n 1) / $(df -h / | awk '{print $2}' | tail -n 1) \033[1;32m $(df -h / | awk '{print $5}' | tail -n 1) \033[0m"
Check   "RAM            :" "$(free -t -m | grep Mem: | awk '{print $3}') MiB / $(free -t -m | grep Mem: | awk '{print $2}') MiB"
Check   "Date:" "$(date)"

