##Son 30 gün hariç tüm dosyaları göster
find . -iname "*" -mtime +30 -exec ls -lah {} \;


##Son 30 gün içerisinde yazan dosyaları göster
find . -iname "*" -mtime -30 -exec ls -lah {} \; 

##Son 30 gün öncesini sil
find . -iname "*" -mtime +30 -exec rm {} \;