## Test Database Shell Ayarları

#### Database Tarafında Dump işlemi için kullanılan Sh

- [Mongo Dump Dosyası](MongoDump/MongoDump.sh)
 
- [Mysql Dump Dosyası](MysqlDump/MysqlDump.sh)

- [Postgre Dump Dosyası](PostgreDump/PostgreDump.sh)

- [Postgre Bcclip Dump Dosyası](PostgreDump/BcPostgreDump.sh)

- [Solr Açıklama](SolrDump/SolrDump.md)  fixlenecek.