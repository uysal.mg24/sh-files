#echo 'Java Eski Sürümleri Siliniyor'
javacontrol="$(java -version 2>&1 | head -n 1 | awk -F '......"' '{print $2}')"
#'."' tırnaklar içerisindeki noktalar kaç hane öncesini alayım demektir.
javaversion="$(java -version 2>&1 | head -n 1 | awk -F '"' '{print $2}')"


   if [ ! "$javacontrol" == "" ] ; then
		if [ ! "$javacontrol" == "11" ] ; then
			echo "Versiyon" "$javaversion" "Kurulu 11 e Yükseltiliyor..."
			yum remove java-* -y
			sudo dnf install java-11-openjdk-devel -y
			sudo java -version
		else
			echo "Java 11 Kurulu Versiyon :" "$javaversion"
		fi
	else
		sudo dnf install java-11-openjdk-devel -y
		sudo java -version
	fi	