#!/bin/bash

try() {
  "$@"
  local status=$?
  if [ $status -ne 0 ]; then
    echo "Error: Command $@ failed with exit code $status" >&2
    exit $status
  fi
}

catch() {
  local status=$?
  if [ $status -ne 0 ]; then
    echo "An error occurred: $@" >&2
  fi
}

# Your script goes here
try my_command
catch "An error occurred while running my_command"
